var sizes = [[1000, 40], [1300, 730.0], [1000, 90], [1300, 600], [320, 50], [300, 250], [728, 90]],
	url = '100249493/golcaracol';


googletag.cmd.push(function() {
	var mapping = googletag.sizeMapping()
		.addSize([320, 480], [[320, 50]])
		.addSize([1050, 200], [728, 90])
		.build();

	var mapping_2 = googletag.sizeMapping()
		.addSize([320, 480], [[300, 250]])
		.addSize([1050, 200], [728, 90])
		.build();

	var mapping_3 = googletag.sizeMapping()
		.addSize([320, 480], [[320, 50]])
		.addSize([1050, 200], [1300, 730.0])
		.build();

	googletag.defineSlot(url, sizes, 'pauta-0').
		defineSizeMapping(mapping).
		addService(googletag.pubads());

	googletag.defineSlot(url, sizes, 'pauta-1').
		defineSizeMapping(mapping_2).
		addService(googletag.pubads());

	googletag.defineSlot(url, sizes, 'pauta-2').
		defineSizeMapping(mapping_3).
		addService(googletag.pubads());

	googletag.pubads().enableSingleRequest();
	googletag.enableServices();


	googletag.display('pauta-0');
	googletag.display('pauta-1');
	googletag.display('pauta-2');

	googletag.pubads().addEventListener("slotRenderEnded", function(event) {
  	var id = event.slot.getSlotElementId();
	  if(!event.isEmpty){
	    if ( id == 'pauta-2' ) {
	    	document.querySelector("body").className += " toma_full";
	    }
	  }else{
	    if ( id == 'pauta-2' ) {
	    	document.querySelector("#pauta-2").style.display = "none";
	    }
	  }
	});

});
