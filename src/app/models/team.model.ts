export class Team {
 id: number;
 name: string;
 nameAbr: string;
 cod: number;
}
