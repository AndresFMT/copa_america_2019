import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import {NgModule} from '@angular/core';
import {ServerModule, ServerTransferStateModule} from '@angular/platform-server';
import {ModuleMapLoaderModule} from '@nguniversal/module-map-ngfactory-loader';

@NgModule({
 bootstrap: [AppComponent],

 imports: [
  AppModule,
  ServerModule,
  ModuleMapLoaderModule,
  ServerTransferStateModule, // comment
 ]
})
export class AppServerModule {}
