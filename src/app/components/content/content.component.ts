import { Component, OnInit, ElementRef } from '@angular/core';
import { Team } from '../../models/team.model';
import { Script } from '../../services/app.service.script';
import { FormControl } from '@angular/forms';
import { ServicesService } from '../../services/services.service';
import { ResponsiveService } from '../../services/responsive.service';
import { MatRadioButton, MatRadioChange } from '@angular/material/radio';
import { Source } from 'webpack-sources';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html'
})
export class ContentComponent implements OnInit {
  teams: Team[] = [];
  loaderScript = new Script();
  isMobile: boolean;
  selects = [];
  octavos = [];
  datos = [];
  located = ['pos1', 'pos2', 'pos3'];
  keys = [];
  selected = -1;
  //inputActives = { "0": [{ "line": "", "posicion": "", "nombre": "", "name_Abr": "", "codigo": 0, "grupo": 0 }], "1": [{ "line": "", "posicion": "", "nombre": "","name_Abr": "", "codigo": 0, "grupo": 0 }], "2": [{ "line": "", "posicion": "", "nombre": "","name_Abr": "", "codigo": 0, "grupo": 0 }] };
  inputActives = [[], [], []];
  indeterminate = false;
  showActions = false;
  a: string;
  constructor(
    private service: ServicesService,
    private elementRef: ElementRef,
    private responsiveService: ResponsiveService
  ) {
    this.resize();
    this.responsiveService.checkWidth();
  }

  ngOnInit() {
    this.getTeamList();
    this.loaderScript.loadScript('assets/js/advertisingDetail.js');
    for ( let i = 0; i < 8; i++) {
      this.selects[i] = [[], [], []];
    }
  }

  resize() {
    this.responsiveService.getMobileStatus().subscribe(isMobile => {
      this.isMobile = isMobile;
    });
  }

  getTeamList(): void {
    this.service.getTeamList().subscribe((teams: Team[]) => {
      for (const key in teams) {
        if (teams.hasOwnProperty(key)) {
          this.teams.push(teams[key]);
          this.keys.push(key);
        }
      }
    });
  }

  onChange($event, item, group, pos, i) {
    const active = this.inputActives[group];

    if (active.length === 0) {
      active.push({
        cod: item.cod,
        id: item.id,
        name: item.name,
        nameAbr: item.nameAbr,
        pos: `${pos}`
      });
    } else {
      for (const check in active) {
        if (active.hasOwnProperty(check)) {
          const element = active[check];
          console.log(element);
          if (element.id === item.id) {
            active[check] = {
              cod: item.cod,
              id: item.id,
              name: item.name,
              nameAbr: item.nameAbr,
              pos: `${pos}`
            };
            console.log('cambio de posicion');
          } else {
            active.push({
              cod: item.cod,
              id: item.id,
              name: item.name,
              nameAbr: item.nameAbr,
              pos: `${pos}`
            });
          }

          // if (element.posicion === pos && element.grupo === group) {
            // this.elementRef.nativeElement.querySelector('.i' + `${i}${element.codigo}`).checked = false;
          // }
        }
      }
    }

    // const indice = active.findIndex(
    //   x => x.line === $event.target.id || x.posicion === pos && x.grupo === group
    // );
    // if (indice >= 0) {
    //   active.splice(indice, 1);
    //   active[indice] = {
    //     line: $event.target.id,
    //     posicion: pos,
    //     nombre: name,
    //     name_Abr: nameAbr,
    //     codigo: cod,
    //     grupo: group
    //   };
    // } else {
    //   // $event.path[0].checked = true;
    // }
    // for (const key in this.inputActives) {
    //   if (this.inputActives.hasOwnProperty(key)) {
    //     const element = this.inputActives[key];
    //     for (const obj in element) {
    //       if (element.hasOwnProperty(obj)) {
    //         const ele = element[obj];
    //       }
    //     }
    //   }
    // }
    console.log(active);
    // console.log(this.inputActives[0][0].posicion);
    // for (const key in this.inputActives) {
    //   if (this.inputActives.hasOwnProperty(key)) {
    //     const element = this.inputActives[key];
    //     // console.log(key);
    //     console.log(element);
    //     for (const obj in element) {
    //       if (element.hasOwnProperty(obj)) {
    //         const ele = element[obj];
    //         console.log(ele.nombre);
    //         const cadena = ele.nombre.substr(0, 3);
    //         console.log(cadena);
    //       }
    //     }
    //   }
    // }

    // console.log(this.inputActives);
    // console.log(this.inputActives[0][0].nombre);
    // if (this.inputActives['0'][0]) {
    //   // this.octavos[0][0];
    // }
    // console.log(value);
    // for (const key in active) {
    //   if (active.hasOwnProperty(key)) {
    //     const element = active[key];
    //     //console.log(element.posicion);
    //     if(element.line == $event.target.id || element.posicion == pos) {
    //       active[key] =  {line: $event.target.id, posicion: pos};
    //       alert('pilas que hay uno repetido');
    //     } else {
    //       alert('Add');
    //       active.push({ line: $event.target.id, posicion: pos });
    //     }
    //   }
    // }

    // if (!active) {
    //   console.log('existe');
    // } else {

    //   //console.log(pos, active);
    // }
    // console.log($event.target.value + 'evento');
    // console.log($event.target.id + 'evento');
    // if (Object.keys(active).length === 0) {
    //   active.push({name: $event.target.value, value: $event.target.id, posicion: pos});
    // } else {
    //   let T = 0;
    //   for (const key in active) {
    //     if (active.hasOwnProperty(key)) {
    //       const element = active[key];
    //       if (element.value === $event.target.id) {
    //         element.name = $event.target.value;
    //       } else {
    //         T += 1;
    //         if (T === active.length) {
    //           active.push({name: $event.target.value, value: $event.target.id});
    //         }
    //       }
    //     }
    //   }
    // }
  }
  reset() {}
}
