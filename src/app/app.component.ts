import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ServicesService } from './services/services.service';
import { ResponsiveService } from './services/responsive.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'Copa2019';

  constructor(public res: ServicesService, private responsiveService: ResponsiveService) {}

  ngOnInit() {
    this.responsiveService.getMobileStatus().subscribe( isMobile => {
      if (isMobile) {
        console.log('Mobile device detected');
      } else {
        console.log('Desktop detected');
      }
    });
    this.onResize();
  }
  onResize() {
    this.responsiveService.checkWidth();
  }
}
