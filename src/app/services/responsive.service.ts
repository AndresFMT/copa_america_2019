import { Injectable, Inject } from '@angular/core';
import { Subject, BehaviorSubject, Observable  } from 'rxjs';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
// import { Observable } from 'rxjs/Observable';

@Injectable()

export class ResponsiveService {
  private isMobile = new Subject();
  public screenWidth: string;
  // public window: any;

 constructor() {
  this.checkWidth();
 }

 onMobileChange(status: boolean) {
  this.isMobile.next(status);
 }

 getMobileStatus(): Observable<any> {
  return this.isMobile.asObservable();
 }

 public checkWidth() {
  const width = window.innerWidth;
  if (width <= 768) {
   this.screenWidth = 'sm';
   this.onMobileChange(true);
  } else if (width > 768 && width <= 1024) {
   this.screenWidth = 'md';
   this.onMobileChange(true);
  } else {
   this.screenWidth = 'lg';
   this.onMobileChange(false);
  }
 }
}
