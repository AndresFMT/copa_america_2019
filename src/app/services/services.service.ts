import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Team } from '../models/team.model';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http: HttpClient) { }

  /**
   * getTeamList
   */
  public getTeamList(): Observable<Team[]> {
    return (this.http.get<Team[]>('assets/data/groups.json'));
  }
}
